
-- SUMMARY --
This is a utility / support module that simplifies SELECT, INSERT, UPDATE and
DELETE operations for tables with a simple primary key (either an auto_increment
key or a key defined in the sequences table).

Some features:
* Automatically serializes objects and arrays
* Caches table information for fast acces

Limitations:
* MySQL support only

For a full description of the module, visit the project page:
  http://drupal.org/project/crud

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/crud

-- USAGE --

The functions are as follows:

$object = crud_get('tablename', $id);
$object = crud_find('tablename', array('field' => 'value', 'field2' => 'value2', ...));
$id = crud_save('tablename', $array_or_object);
crud_delete('tablename', $id);

